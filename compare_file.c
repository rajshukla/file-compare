// This program compares the two binary files.

#include <stdio.h>
#include <string.h>
#include <time.h>

int compare_files(FILE *, FILE *);

int main (int argc, char *argv[])
{

    FILE *fp1, *fp2;
    clock_t start_time, end_time;
    double time_taken;
    int comp;
    
    start_time = clock();
    if (argc < 3)  // Check for number of arguments.
    {
        printf ("Insufficient arguments\n");
        return 0;
    }
    
    fp1 = fopen(argv[1], "rb");
    
    if (fp1 == NULL)  // Check for file 1
    {
        printf("\nError in opening file %s", argv[1]);
        return 0;
    }
    
    fp2 = fopen(argv[2], "rb");
    
    if (fp2 == NULL) // Check for file 2
    {
        printf("\nError in opening file %s", argv[2]);
        return 0;
    }
    
    comp = compare_files(fp1, fp2); // Call function to compare files
    if (comp == 1)
    {
        printf("%s and %s are not equal\n", argv[1], argv[2]);
    }
    else if  (comp == 2)
    {
        printf("%s and %s are equal\n", argv[1], argv[2]);
    }
    end_time = clock();
    time_taken = (((double)(end_time - start_time))/CLOCKS_PER_SEC)*1000;
    printf("Time taken to compare files = %f msec \n", time_taken);
    return 0;
    
}



//Function to compare files.
int compare_files(FILE *fp1, FILE *fp2)
{
    char tmp1[256], tmp2[256];
    int n1, n2, n_min, i;
    
    
    do
    {
        n1 = fread(tmp1, sizeof *tmp2, sizeof tmp1/ sizeof *tmp1, fp1);
        n2 = fread(tmp2, sizeof *tmp2, sizeof tmp2/ sizeof *tmp2, fp2);
        
        if (n1 < n2)
        {
            n_min = n1;
        }
        else
        {
            n_min = n2;
        }
        
        if (memcmp(tmp1, tmp2, n_min))
        {
            for (i = 0; i < n_min; i++)
            {
                if (tmp1[i]!= tmp2[2])
                {
                    return 1;
                }
            }
        }
        
        
        if (n1 > n_min || n2 > n_min)
        {
            return 1;
        }
    }
    while(n1);
    
    
    return 2; 
}














